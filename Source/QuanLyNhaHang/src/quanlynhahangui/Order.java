package quanlynhahangui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import quanlynhahang.model.Ban;
import quanlynhahang.model.BanService;
import quanlynhahang.model.DanhMuc;
import quanlynhahang.model.DanhMucBan;
import quanlynhahang.model.DanhMucBanService;
import quanlynhahang.model.DanhMucService;
import quanlynhahang.model.SanPham;
import quanlynhahang.model.SanPhamService;

public class Order extends JFrame {
	JPanel  pnCenterOfBot;
	JComboBox<DanhMuc> cboDanhMuc;
	JButton btnOrder;
	Vector<DanhMuc> dsDM;
	Vector<SanPham> dsSp;
	DanhMuc dm;
	SanPham sp;
	public Order(String title)
	{
		super(title);
		addControls();
		addEvents();
	}
	public void addControls()
	{
		Container con=getContentPane();
		con.setLayout(new BorderLayout());
		JPanel pnTop = new JPanel();
		pnTop.setLayout(new BorderLayout());
		pnTop.setPreferredSize(new Dimension(0, 100));
		JPanel pnBottom = new JPanel();
		pnBottom.setLayout(new BorderLayout());
		JSplitPane sp = new JSplitPane(JSplitPane.VERTICAL_SPLIT,pnTop,pnBottom);
		sp.setOneTouchExpandable(true);
		con.add(sp,BorderLayout.CENTER);
		JPanel pnT = new JPanel();
		pnT.setPreferredSize(new Dimension(0, 40));
		pnTop.add(pnT,BorderLayout.NORTH);
		
		JPanel pnTieuDe = new JPanel();
		pnTieuDe.setLayout(new FlowLayout());
		JLabel lblTieuDe = new JLabel();
		lblTieuDe.setText("Order");
		lblTieuDe.setBackground(Color.LIGHT_GRAY);
		Font fontTieuDe = new Font("arial",Font.PLAIN,20);
		lblTieuDe.setFont(fontTieuDe);
		pnTieuDe.add(lblTieuDe);
		pnTop.add(pnTieuDe,BorderLayout.CENTER);
		
		JPanel pnDM=new JPanel();
		pnDM.setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblDM=new JLabel("Danh mục:");
		cboDanhMuc=new JComboBox();
		pnDM.add(lblDM);
		pnDM.add(cboDanhMuc);
		pnBottom.add(pnDM,BorderLayout.NORTH);
		
		pnCenterOfBot=new JPanel();
		pnCenterOfBot.setLayout(new GridLayout(6,5));
		JScrollPane crBottom=new JScrollPane(pnCenterOfBot, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		pnBottom.add(crBottom,BorderLayout.CENTER);
		
		JPanel pnBotOfBot=new JPanel();
		pnBotOfBot.setLayout(new FlowLayout(FlowLayout.RIGHT));
		btnOrder=new JButton("Order");
		pnBotOfBot.add(btnOrder);
		pnBottom.add(pnBotOfBot,BorderLayout.SOUTH);
	}
	public void addEvents()
	{
		HienThiDanhMuc();
		//HienThiSanPham(dsSp);
		cboDanhMuc.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (cboDanhMuc.getSelectedIndex() == -1)
					return;
				dm = (DanhMuc) cboDanhMuc.getSelectedItem();
				SanPhamService spSv = new SanPhamService();
				dsSp = spSv.DanhSachHangHoa(dm.getMaDM());
				pnCenterOfBot.removeAll();
				HienThiSanPham(dsSp);
				pnCenterOfBot.repaint();
				pnCenterOfBot.revalidate();
			}
		});
	}
	protected void HienThiSanPham(Vector<SanPham> dsSp) {
		// TODO Auto-generated method stub
		for (final SanPham sanPham : dsSp) {
			JPanel pn = new JPanel();
			pn.setLayout(new BoxLayout(pn, BoxLayout.Y_AXIS));
			JPanel p = new JPanel();
			pn.add(p);
			final JButton btn = new JButton();
			btn.setPreferredSize(new Dimension(75, 75));
			btn.setIcon(new ImageIcon(sanPham.getImage()));
			JLabel lbl = new JLabel(sanPham.getTenSP());
			lbl.setAlignmentX(CENTER_ALIGNMENT);
			p.add(btn);
			pn.add(lbl);
			sp = sanPham;
			pnCenterOfBot.add(pn);
			/*btn.addActionListener(new ActionListener() {
				
				public void actionPerformed(ActionEvent e) {
					// TODO Auto-generated method stub
					txtMaBan.setText(ban.getMaBan());
					txtTenBan.setText(ban.getTenBan());
					txtSoCho.setText(ban.getSoCho());
				}
			});*/
		}
	}
	private void HienThiDanhMuc() {
		// TODO Auto-generated method stub
		DanhMucService dmSv=new DanhMucService();
		dsDM=dmSv.DanhSachDanhMuc();
		for (DanhMuc d : dsDM) {

			cboDanhMuc.addItem(d);
		}
		SanPhamService spSv = new SanPhamService();
		dsSp = spSv.DanhSachHangHoa(1);
		HienThiSanPham(dsSp);
	}
	public void showWindow() {
		this.setSize(600, 600);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		this.setVisible(true);

	}
}
