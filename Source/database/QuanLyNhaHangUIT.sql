﻿CREATE Database QuanLyNhaHangUIT
CREATE TABLE User1
(
MaUser integer PRIMARY KEY,
TenUser nvarchar(100),
PassWord1  nvarchar(50),
Administrator nvarchar(100)

)

Insert into User1(MaUser,TenUser,PassWord1,Administrator) Values
(1,'Admin','Admin','Admin'),
(2,'NV','NV','NV'),
(3,'Bep','Bep','Bep')

CREATE TABLE DanhMuc
(
MaDM integer PRIMARY KEY,
TenDM nvarchar(100)
)

CREATE TABLE SanPham
(
MaSP integer PRIMARY KEY,
TenSP nvarchar(100),
DVT nvarchar(50),
DonGia integer,
MaDM integer
ConStraint FR_SP FOREIGN KEY(MaDM) REFERENCES DanhMuc(MaDM)
)

CREATE TABLE DanhMucBan
(
MaDMB integer PRIMARY KEY,
TenDMB nvarchar(100)
)

CREATE TABLE Ban
(
MaBan nvarchar(10) PRIMARY KEY,
TenBan nvarchar(100),
SoCho integer,
MaDMB integer,
Status2 integer DEFAULT 0
ConStraint FR_DMBAN FOREIGN KEY (MaDMB) REFERENCES DanhMucBan(MaDMB)
)


CREATE TABLE NhanVien
(
	MaNV varchar(20) PRIMARY KEY,
	TenNV nvarchar(100),
	GioiTinh nvarchar(20),
	DiaChi nvarchar(200),
	SDT nvarchar(20),
	NgayVaoLam nvarchar(20),
	CongViec nvarchar(100)
)

CREATE TABLE HoaDon
(
	MaHD integer PRIMARY KEY,
	MaBan nvarchar(10),
	MaNV  varchar(20),
	NgayHD nvarchar(200),
	Status1 nvarchar(20),
	ConStraint FR_NV FOREIGN KEY(MaNV) REFERENCES NhanVien(MaNV),
	ConStraint FR_BAN FOREIGN KEY (MaBan) REFERENCES Ban(MaBan)
)

CREATE TABLE ChiTietHoaDon
(
	MaHD integer,
	MaSP integer,
	Soluong  integer,
	ThanhTien nvarchar(200)
	ConStraint PK PRIMARY KEY(MaHD,MaSP),
	ConStraint FR_HD FOREIGN KEY(MaHD) REFERENCES HoaDon(MaHD),
	ConStraint FR_SPP FOREIGN KEY(MaSP) REFERENCES SanPham(MaSP),

)
ALTER TABLE NhanVien
ADD Image nvarchar(100);
Insert into DanhMucBan(MaDMB,TenDMB) values
(1,'VIP'),
(2,'THƯỜNG')
Insert into Ban(MaBan,TenBan,SoCho,MaDMB,Status2) values
('V1','Bàn V1',7,1,0),
('V2','Bàn V1',7,1,0),
('V3','Bàn V1',5,1,0),
('V4','Bàn V1',5,1,0),
('V5','Bàn V1',10,1,0),
('V6','Bàn V1',10,1,0),
('V7','Bàn V1',10,1,0),
('V8','Bàn V1',7,1,0),
('V9','Bàn V1',5,1,0),
('V10','Bàn V1',7,1,0),
('T1','Bàn V1',7,2,0),
('T2','Bàn V1',7,2,0),
('T3','Bàn V1',5,2,0),
('T4','Bàn V1',5,2,0),
('T5','Bàn V1',10,2,0),
('T6','Bàn V1',10,2,0),
('T7','Bàn V1',10,2,0),
('T8','Bàn V1',7,2,0),
('T9','Bàn V1',5,2,0),
('T10','Bàn V1',7,2,0)
 Delete From Ban
 Insert into Ban(MaBan,TenBan,SoCho,MaDMB,Status2) values
('V1','Bàn V1',7,1,0),
('V2','Bàn V2',7,1,0),
('V3','Bàn V3',5,1,0),
('V4','Bàn V4',5,1,0),
('V5','Bàn V5',10,1,0),
('V6','Bàn V6',10,1,0),
('V7','Bàn V7',10,1,0),
('V8','Bàn V8',7,1,0),
('V9','Bàn V9',5,1,0),
('V10','Bàn V10',7,1,0),
('T1','Bàn T1',7,2,0),
('T2','Bàn T2',7,2,0),
('T3','Bàn T3',5,2,0),
('T4','Bàn T4',5,2,0),
('T5','Bàn T5',10,2,0),
('T6','Bàn T6',10,2,0),
('T7','Bàn T7',10,2,0),
('T8','Bàn T8',7,2,0),
('T9','Bàn T9',5,2,0),
('T10','Bàn T10',7,2,0)
ALTER TABLE SanPham
ADD Image nvarchar(100);
Alter table SanPham
Drop Column Image;
Alter Table SanPham
Add Image nvarchar(100);
Alter table SanPham
Drop Column Image;
Alter Table SanPham
Add ImageSP nvarchar(100);